#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <QFile>
#include <QDataStream>
#include <QDateTime>
#include <QVector>
#include "task.h"

/** NIS Reminders
  Author: Niklas Schachl
  Date: 17.4.2022
 */
class FileHandler
{
    QString path;

public:
    FileHandler();
    //void write(Task);
    bool write(QVector<Task>);
    QVector<Task> read();
};

#endif // FILEHANDLER_H
