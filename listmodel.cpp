#include "listmodel.h"

/** NIS Reminders
  Author: Niklas Schachl
  Date: 5.5.2022
 */
ListModel::ListModel(QObject *parent) : QAbstractListModel(parent)
{
    fileHandler = new FileHandler();
    //m_datas = fileHandler->read();

    Task  xx("Klimmzüge,", QDateTime::currentDateTime());
    Task  xx2("Seterra,", QDateTime::currentDateTime());

    m_datas << xx << xx2;
}

void ListModel::Add(Task& task)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_datas.push_back(task);
    endInsertRows();
}

void ListModel::insertTask(Task task)
{
    m_datas.push_back(task);
}

void ListModel::clear()
{
    // clear screen will not display rows
    beginRemoveRows(QModelIndex(), 0, m_datas.size());

    // Clear the dynamic array
    m_datas.clear();
    endRemoveRows();
    //end
}

// add an external interface QML call data in the specified row
void  ListModel::minsert(int index, const QString& data1, const QDateTime& data2)
{
    Task d(data1, data2);
    beginInsertRows(QModelIndex(), index, index);
    m_datas.insert(m_datas.begin()+index, d);
    endInsertRows();
}

void  ListModel::mremove(int index)
{
    beginRemoveRows(QModelIndex(), index, index);
    m_datas.erase(m_datas.begin() + index);
    endRemoveRows();
}

void ListModel::pushdata(const QString& data1, const QDateTime& data2)
{
    Task d(data1, data2);
    Add(d);
}

int ListModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_datas.size();
}



QVariant ListModel::data(const QModelIndex &index, int role)  const
{
    if (index.row() < 0 || index.row() >= m_datas.size())
    {
        return QVariant();
    }
    const Task& d = m_datas[index.row()];
    if (role == datatype::type1)
    {
        return d.data1();

    }
    else if (role == datatype::type2)
    {
        return d.data2AsString();
    }
    else if (role == datatype::type3)
    {
        return d.obj;
    }
    return QVariant();
}

// definition data alias QHash <int, QByteArray> parent predetermined
QHash<int, QByteArray> ListModel::roleNames() const
{
    QHash<int, QByteArray>  d;
    d[datatype::type1] = "data1";
    d[datatype::type2] = "data2";
    d[datatype::type3] = "curitem";
    return  d;
}
