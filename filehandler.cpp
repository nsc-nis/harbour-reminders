#include "filehandler.h"

/** NIS Reminders
  Author: Niklas Schachl
  Date: 21.4.2022
 */
FileHandler::FileHandler()
{
    path = "$XDG_CONFIG_HOME/at.nis/reminders/tasklist.niscfg";
}

bool FileHandler::write(QVector<Task> tasks)
{
    QFile file(path);
    if(file.open(QIODevice::WriteOnly))
    {
        QDataStream out(&file);             // we will serialize the data into the file
        out << tasks;                       // serialize our tasklist
        file.close();                       //close the file
        return true;
    }
    return false;
}

QVector<Task> FileHandler::read()
{
    QVector<Task> tasks;
    QFile file(path);
    if(file.open(QIODevice::ReadOnly))
    {
        QDataStream in(&file);              // read the data serialized from the file
        in >> tasks;
        return tasks;
    }
    return tasks;
}
