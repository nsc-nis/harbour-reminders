#ifndef LISTMODEL_H
#define LISTMODEL_H

#include <QAbstractListModel>
#include "filehandler.h"
#include "task.h"
#include <QHash>
#include <QList>
#include <QByteArray>
#include <QDebug>
#include <vector>


/** NIS Reminders
  Author: Niklas Schachl
  Date: 5.5.2022
 */
class ListModel : public QAbstractListModel
{
    Q_OBJECT
    FileHandler *fileHandler;

public:
    enum datatype
    {
            type1=1,
            type2,
            type3
    };

    ListModel(QObject *parent = 0);

    // add an external interface QML call data
    Q_INVOKABLE void pushdata(const QString& data1, const QDateTime& data2);

    // add an external interface QML call data in the specified row
    Q_INVOKABLE void  minsert(int index, const QString& data1, const QDateTime& data2);

    // external interface to delete the specified line
    Q_INVOKABLE void  mremove(int index);

    // add an external interface to call C ++ data
    void  Add(Task&  md);

    // Clear the external interface model
    Q_INVOKABLE void clear();

    // internal virtual function call to get the contents of the index qml line role content index
    QVariant data(const QModelIndex &index, int role =Qt::DisplayRole) const;

    // virtual function Gets the number of model lines
    int rowCount(const QModelIndex &parent  = QModelIndex() ) const;

    // alias qml internal contents of the virtual function call
    QHash<int, QByteArray> roleNames()  const;

    // custom settings of the current model of the current row index pointer assembly
   Q_INVOKABLE void setcuritem(int index , QVariant  j)
   {
       //qDebug()<<j;
       m_datas[index].obj = j;
   }

    //return number of tasks
    Q_INVOKABLE int getTaskNr() {return m_datas.size();}
    void insertTask(Task);

   ~ListModel() {}

private:
   QVector<Task> m_datas;
};

#endif // LISTMODEL_H
