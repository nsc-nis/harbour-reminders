import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.at.nis 1.0

/** NIS Reminders - Main Page
  Author: Niklas Schachl
  Date: 5.5.2022
 */
Page
{
    id: page
    orientation: Orientation.Portrait

    SilicaListView
    {
         anchors.fill: parent

         header: Column
         {
             width: parent.width
             height: header.height + mainColumn.height + Theme.paddingLarge

             PageHeader
             {
                 id: header
                 title: qsTr("Reminders")
             }

             Column
             {
                 id: mainColumn
                 width: parent.width
                 spacing: Theme.paddingLarge
             }
         }

         PullDownMenu
         {
             MenuItem
             {
                 text: qsTr("New Task")
                 onClicked: pageStack.animatorPush(Qt.resolvedUrl("NewTaskPage.qml"))
             }
         }

          model: ListModel
          {
              id: listModel
          }

          delegate: BackgroundItem
          {
              DetailItem
              {
                  id: listEntry
                  width: parent.width

                   label: data1
                   value: data2
                   x: Theme.horizontalPageMargin
                   anchors.verticalCenter: parent.verticalCenter

                    ContextMenu
                    {
                        MenuItem
                        {
                            text: qsTr("Remove")
                            onClicked:
                            {
                                listEntry.remorseAction(qsTr("Deleting..."), function()
                                {
                                    listModel.remove(model.index)
                                })
                            }
                        }
                    }
              }


               /*
               Component.onCompleted:
               {
                  listModel.setcuritem(index,this)
                  console.log(curitem)
               }
               */


               //menu: ContextMenu


          }
          /*
          Component.onCompleted:
          {
              for(var x =0;x<100;x++)
              {
                  listModel.pushdata(x+"a,",x+"b");
              }
          }
          */
     }
}
