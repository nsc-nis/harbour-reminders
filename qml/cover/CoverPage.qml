import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.at.nis 1.0

/** NIS Reminders - Cover Page
  Author: Niklas Schachl
  Date: 17.4.2022
 */
CoverBackground
{   
    property int aNumber

    Label
    {
        id: label
        anchors.centerIn: parent
        text: qsTr("Current tasks: ") + listModel.getTaskNr()
    }

    ListModel
    {
        id: listModel
    }

    CoverActionList
    {
        id: coverAction

        CoverAction
        {
            iconSource: "image://theme/icon-cover-new"
            onTriggered: pageStack.animatorPush(Qt.resolvedUrl("NewTaskPage.qml"))
        }
    }
}
