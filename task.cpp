#include "task.h"

Task::Task(QString name, QDateTime dateTime)
{
    this->name = name;
    this->dateTime = dateTime;
}

Task::Task()
{
    name = "";
}

QString Task::getName()
{
    return name;
}

QDateTime Task::getDateTime()
{
    return dateTime;
}

void Task::setName(QString name)
{
    this->name = name;
}

void Task::setDateTime(QDateTime dateTime)
{
    this->dateTime = dateTime;
}
