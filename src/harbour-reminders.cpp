#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>
#include "listmodel.h"
#include "task.h"

int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/harbour-reminders.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //   - SailfishApp::pathToMainQml() to get a QUrl to the main QML file
    //
    // To display the view, call "show()" (will show fullscreen on device).
    ListModel * m = new ListModel();
    Task  xx("Klimmzüge,", QDateTime::currentDateTime());
    m->Add(xx);

    Task  xx2("Seterra,", QDateTime::currentDateTime());
    m->Add(xx2);

    /*
    qmlRegisterType<ListModel>("harbour.at.nis", 1, 0, "ListModel");

    return SailfishApp::main(argc, argv);
    */

    // Set up QML engine.
    QScopedPointer<QGuiApplication> app(SailfishApp::application(argc, argv));
    QScopedPointer<QQuickView> v(SailfishApp::createView());

    qmlRegisterType<ListModel>("harbour.at.nis", 1, 0, "ListModel");

    // Start the application.
    v->setSource(SailfishApp::pathTo("qml/harbour-reminders.qml"));
    v->show();

    return app->exec();
}
