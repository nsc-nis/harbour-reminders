#ifndef TASK_H
#define TASK_H

#include <QDateTime>
#include <QVariant>

class Task : public QVariant
{
    Q_PROPERTY(QString name READ getName WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QDateTime dateTime READ getDateTime WRITE setDateTime NOTIFY dateChanged)

public:
    QVariant  obj;
    Task(QString, QDateTime);
    Task();
    QString getName();
    QDateTime getDateTime();
    void setName(QString);
    void setDateTime(QDateTime);
    QString  data1() const { return this->name; }
    QString  data2AsString() const { return this->dateTime.toString(); }
    QDateTime data2() const { return this->dateTime; }

private:
    QString name;
    QDateTime dateTime;
};

#endif // TASK_H
